package requests

import (
	"encoding/json"
	validation "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/create_acc/resources"
	"net/http"
)

type CreateAcc struct {
	Data resources.Acc
}

func newCreateAcc(acc resources.Acc) *CreateAcc {
	return &CreateAcc{
		Data: acc,
	}
}

func NewCreateAcc(r *http.Request) (*CreateAcc, error) {
	var request resources.AccResponse

	err := json.NewDecoder(r.Body).Decode(&request)
	res := newCreateAcc(request.Data)
	if err != nil {
		return res, errors.Wrap(err, "failed to unmarshal")
	}

	return res, res.validate()
}

func (r *CreateAcc) validate() error {
	return validation.Errors{
		"data/id": validation.Validate(r.Data.ID, validation.Required),
		"data/attributes/keys": validation.Validate(r.Data.Attributes.Keys, validation.Required),
	}
}

func (r *CreateAcc) Acc() (*resources.Acc, error) {
	keys := r.Data.Attributes.Keys
	accType := r.Data.Type
	id := r.Data.ID

	attrib := resources.AccAttributes{
		Keys: keys,
	}
	key := resources.Key{
		ID: id,
		Type: accType,
	}

	return &resources.Acc{
		Key: key,
		Attributes: attrib,
	}, nil
}