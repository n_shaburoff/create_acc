package handlers

import (
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/tokend/create_acc/internal/service/requests"
	"gitlab.com/tokend/create_acc/resources"
	"net/http"
)

func CreateAcc(w http.ResponseWriter, r *http.Request) {

	request, err := requests.NewCreateAcc(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	acc, err := request.Acc()
	if err != nil {
		Log(r).WithError(err).Error("failed to get blob data from request")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}


	response := resources.AccResponse{
		Data: *acc,
	}

	ape.Render(w, response)
}

