/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type Acc struct {
	Key
	Attributes AccAttributes `json:"attributes"`
}
type AccResponse struct {
	Data     Acc      `json:"data"`
	Included Included `json:"included"`
}

type AccListResponse struct {
	Data     []Acc    `json:"data"`
	Included Included `json:"included"`
	Links    *Links   `json:"links"`
}

// MustAcc - returns Acc from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustAcc(key Key) *Acc {
	var acc Acc
	if c.tryFindEntry(key, &acc) {
		return &acc
	}
	return nil
}
