module gitlab.com/tokend/create_acc

go 1.16

require (
	github.com/alecthomas/kingpin v2.2.6+incompatible
	github.com/certifi/gocertifi v0.0.0-20200922220541-2c3bb06c6054 // indirect
	github.com/getsentry/raven-go v0.2.0 // indirect
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible // indirect
	github.com/gobuffalo/packr/v2 v2.8.1
	github.com/google/jsonapi v0.0.0-20210306203318-b10ff4bf785b // indirect
	github.com/rubenv/sql-migrate v0.0.0-20210215143335-f84234893558
	gitlab.com/distributed_lab/ape v1.5.0
	gitlab.com/distributed_lab/kit v1.8.5
	gitlab.com/distributed_lab/logan v3.8.0+incompatible
	gitlab.com/distributed_lab/lorem v0.2.0 // indirect
)
