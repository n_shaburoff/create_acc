package main

import (
	"os"

  "gitlab.com/tokend/create_acc/internal/cli"

)

func main() {
	if !cli.Run(os.Args) {
		os.Exit(1)
	}
}
