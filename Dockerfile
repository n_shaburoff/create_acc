FROM golang:1.12

WORKDIR /go/src/gitlab.com/tokend/create_acc

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o /usr/local/bin/create_acc gitlab.com/tokend/create_acc


###

FROM alpine:3.9

COPY --from=0 /usr/local/bin/create_acc /usr/local/bin/create_acc
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["create_acc"]
